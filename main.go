package main

import (
	"context"
	"infrastructure/topics/bff"
	"os"

	"cloud.google.com/go/pubsub"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
	"google.golang.org/api/iterator"
)

func main() {
	if err := godotenv.Load(); err != nil {
		log.Error().Err(err).Send()
		os.Exit(0)
	}
	ctx := context.Background()
	client := GetPubSubClient(ctx)
	bff.Setup(ctx, client)
	PrintTopicList(ctx, client)
}

func GetPubSubClient(ctx context.Context) *pubsub.Client {
	client, err := pubsub.NewClient(ctx, os.Getenv("GCP_PROJECT_ID"))
	if err != nil {
		log.Error().Err(err).Send()
		os.Exit(0)
	}
	return client
}

func PrintTopicList(ctx context.Context, client *pubsub.Client) {
	it := client.Topics(ctx)
	for {
		topic, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Error().Err(err).Send()
		}
		log.Info().Str("topicID", topic.ID()).Send()
		PrintTopicSubscriptionList(ctx, topic)
	}
}

func PrintTopicSubscriptionList(ctx context.Context, topic *pubsub.Topic) {
	it := topic.Subscriptions(ctx)
	for {
		sub, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Error().Err(err).Send()
		}
		log.Info().
			Str("topicID", topic.ID()).
			Str("subscription", sub.ID()).
			Send()
	}
}
