package bff

import (
	"context"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/rs/zerolog/log"
)

func MagicLinkLoginRequested(
	ctx context.Context,
	client *pubsub.Client,
	topic *pubsub.Topic) {
	_, err := client.CreateSubscription(ctx,
		"magic-link-login-requested",
		pubsub.SubscriptionConfig{
			Topic:       topic,
			AckDeadline: 20 * time.Second,
			Filter:      `attributes.EventType="MagicLinkLoginRequested"`,
		})
	if err != nil {
		log.Error().Err(err).Send()
	}
}
