package bff

import (
	"context"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/rs/zerolog/log"
)

func FrontierCreationRequested(
	ctx context.Context,
	client *pubsub.Client,
	topic *pubsub.Topic) {
	_, err := client.CreateSubscription(ctx,
		"frontier-creation-requested",
		pubsub.SubscriptionConfig{
			Topic:       topic,
			AckDeadline: 20 * time.Second,
			Filter:      `attributes.EventType="FrontierCreationRequested"`,
		})
	if err != nil {
		log.Error().Err(err).Send()
	}
}
