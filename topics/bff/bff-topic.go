package bff

import (
	"context"
	"os"

	"cloud.google.com/go/pubsub"
	"github.com/rs/zerolog/log"
)

func Setup(ctx context.Context, c *pubsub.Client) {
	topicName := "fastarmor-bff-topic"
	t := c.Topic(topicName)
	exists, err := t.Exists(ctx)
	if err != nil {
		log.Info().Err(err).Send()
		os.Exit(0)
	}
	if !exists {
		t, err = c.CreateTopic(ctx, topicName)
	}
	if err != nil {
		log.Info().Err(err).Send()
		os.Exit(0)
	}
	FrontierCreationRequested(ctx, c, t)
	MagicLinkLoginRequested(ctx, c, t)
}
